/**
 * Bootstrap
 */
import bootstrap from 'bootstrap';
/**
 * Imagenes
 */
import './images'
/**
 * Plugins
 */
import "@fortawesome/fontawesome-free/js/all"
/**
 * Css
 */
require('../css/app.scss');