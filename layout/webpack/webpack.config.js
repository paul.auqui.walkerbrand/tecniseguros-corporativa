const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');
const htmlWebpackPartialsPlugin = require('html-webpack-partials-plugin');
const miniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const copyPlugin = require("copy-webpack-plugin");
const minify = {
    collapseWhitespace: true,
    keepClosingSlash: true,
    removeComments: true,
    removeRedundantAttributes: true,
    removeScriptTypeAttributes: true,
    removeStyleLinkTypeAttributes: true,
    useShortDoctype: true
};

/**
 *
 * @type {string[]}
 */

const pages = [
    'index.html', 'presentacion.html', 'principal.html', 'customer_experience.html', 'tecniseguros_web.html', 'tecnimovil.html',
    '1800_ayuda.html', 'tracking_siniestros.html', 'cx-tecniseguros.html', 'asistencia_medica.html',
    'riesgos_empresariales.html', 'certificacion_iso.html', 'lineas-negocio.html', 'seguros-empresariales.html',
    'seguros-vida.html', 'seguros-financieros.html', 'seguros-pymes.html', 'seguros-individuales.html',
    'banca-seguros.html', 'seguros-credito.html', 'fianzas.html', 'indices-financieros.html',
    'indices-financieros-contenido.html', 'nuestros-clientes.html', 'repaldo-institucional.html',
    'nuestro-ranking-brokers.html', 'estructura-servicios.html', 'doctor-linea.html', 'leo-experience.html', 'administracion-contratistas.html', 'charlas.html'
];

/**
 * Module's
 * @type {{test: RegExp, exclude: RegExp, use: {loader: string, options: {presets: string[], plugins: string[]}}}}
 */
const javascriptRules = {
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
        loader: "babel-loader",
        options: {
            presets: [
                '@babel/preset-react',
                '@babel/preset-env'
            ],
            plugins: ['@babel/plugin-proposal-optional-chaining']
        }
    }
};

const cssRules = {
    test: /\.(sa|sc|c)ss$/,
    use: [miniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
    exclude: /node_modules/,
};

const imageRules = {
    test: /\.(jpg|png|git|jpeg)$/,
    use: [{
        loader: 'file-loader',
        options: {
            name: '[name].[ext]',
            outputPath: 'static/',
            useRelativePath: true
        }
    }]
};

const fontsRules = {
    test: /\.(svg|eot|woff|woff2|ttf)$/,
    loader: 'file-loader',
    options: {
        name: '[name].[ext]',
        outputPath: 'fonts/'
    }
};

const handlebars = {
    test: /\.handlebars$/,
    loader: 'handlebars-loader'
};

module.exports = {
    entry: './src/js/app.js',
    stats: {
        children: true,
        errorDetails: true
    },
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: "js/app.[contenthash].js",
    },
    module: {
        rules: [
            javascriptRules,
            imageRules,
            cssRules,
            fontsRules,
            handlebars
        ]
    },
    plugins: [
        new miniCssExtractPlugin({
            filename: 'css/[name]-styles.css'
        }),
        new copyPlugin({
            patterns: [
                {from: "./src/fonts", to: "fonts"},
            ],
        }),
        new htmlWebpackPlugin({
            filename: 'index.html',
            template: './src/views/index.html',
            minify: minify
        }),
        new htmlWebpackPlugin({
            filename: 'cx-tecniseguros.html',
            template: './src/views/cx-tecniseguros.html',
            minify: minify
        }),
        new htmlWebpackPlugin({
            filename: 'customer_experience.html',
            template: './src/views/customer_experience.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'tecniseguros_web.html',
            template: './src/views/tecniseguros_web.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'tecnimovil.html',
            template: './src/views/tecnimovil.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: '1800_ayuda.html',
            template: './src/views/1800_ayuda.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'tracking_siniestros.html',
            template: './src/views/tracking_siniestros.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'certificacion_iso.html',
            template: './src/views/certificacion_iso.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'asistencia_medica.html',
            template: './src/views/asistencia_medica.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'riesgos_empresariales.html',
            template: './src/views/riesgos_empresariales.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'indices-financieros.html',
            template: './src/views/indices-financieros.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'indices-financieros-contenido.html',
            template: './src/views/indices-financieros-contenido.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'nuestros-clientes.html',
            template: './src/views/nuestros-clientes.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'repaldo-institucional.html',
            template: './src/views/repaldo-institucional.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'nuestro-ranking-brokers.html',
            template: './src/views/nuestro-ranking-brokers.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'estructura-servicios.html',
            template: './src/views/estructura-servicios.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'mision-vision-adn.html',
            template: './src/views/mision-vision-adn.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'seguros-empresariales.html',
            template: './src/views/seguros-empresariales.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'seguros-vida.html',
            template: './src/views/seguros-vida.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'seguros-financieros.html',
            template: './src/views/seguros-financieros.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'lineas-negocio.html',
            template: './src/views/lineas-negocio.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'seguros-pymes.html',
            template: './src/views/seguros-pymes.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'seguros-individuales.html',
            template: './src/views/seguros-individuales.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'banca-seguros.html',
            template: './src/views/banca-seguros.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'seguros-credito.html',
            template: './src/views/seguros-credito.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'fianzas.html',
            template: './src/views/fianzas.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'principal.html',
            template: './src/views/principal.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'presentacion.html',
            template: './src/views/presentacion.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'cobertura-geografica.html',
            template: './src/views/cobertura-geografica.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'servicios.html',
            template: './src/views/servicios.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'doctor-linea.html',
            template: './src/views/doctor-linea.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'leo-experience.html',
            template: './src/views/leo-experience.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'administracion-contratistas.html',
            template: './src/views/administracion-contratistas.html',
            template_filename: pages
        }),
        new htmlWebpackPlugin({
            filename: 'charlas.html',
            template: './src/views/charlas.html',
            template_filename: pages
        }),
        new htmlWebpackPartialsPlugin({
            path: path.resolve(__dirname, '../src/views/partials/head.html'),
            location: 'partial-head',
            template_filename: pages
        }),
        new htmlWebpackPartialsPlugin({
            path: path.resolve(__dirname, '../src/views/partials/footer.html'),
            location: 'partial-footer',
            template_filename: pages
        }),
        new htmlWebpackPartialsPlugin({
            path: path.resolve(__dirname, '../src/views/partials/nav.html'),
            location: 'partial-nav',
            template_filename: pages
        }),
        new htmlWebpackPartialsPlugin({
            path: path.resolve(__dirname, '../src/views/partials/menu-cx.html'),
            location: 'menu-cx',
            template_filename: pages
        }),
        new htmlWebpackPartialsPlugin({
            path: path.resolve(__dirname, '../src/views/partials/menu-lineas.html'),
            location: 'menu-lineas',
            template_filename: pages
        }),
        new htmlWebpackPartialsPlugin({
            path: path.resolve(__dirname, '../src/views/partials/cx-script.html'),
            location: 'cx-script',
            template_filename: pages
        }),
        new htmlWebpackPartialsPlugin({
            path: path.resolve(__dirname, '../src/views/partials/indice-financiero-script.html'),
            location: 'indice-script',
            template_filename: pages
        })
    ],
    optimization: {
        minimize: true,
        minimizer: [
            new CssMinimizerPlugin(),
        ],
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9003,
    }
};
