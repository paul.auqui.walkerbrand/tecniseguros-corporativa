const path = require('path');
const miniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const copyPlugin = require("copy-webpack-plugin");

/**
 * Module's
 * @type {{test: RegExp, exclude: RegExp, use: {loader: string, options: {presets: string[], plugins: string[]}}}}
 */
const javascriptRules = {
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
        loader: "babel-loader",
        options: {
            presets: [
                '@babel/preset-react',
                '@babel/preset-env'
            ],
            plugins: ['@babel/plugin-proposal-optional-chaining']
        }
    }
};

const cssRules = {
    test: /\.(sa|sc|c)ss$/,
    use: [miniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
    exclude: /node_modules/,
};

const imageRules = {
    test: /\.(jpg|png|git|jpeg)$/,
    use: [{
        loader: 'file-loader',
        options: {
            name: '[name].[ext]',
            outputPath: 'static/',
            useRelativePath: true
        }
    }]
};

const fontsRules = {
    test: /\.(svg|eot|woff|woff2|ttf)$/,
    loader: 'file-loader',
    options: {
        name: '[name].[ext]',
        outputPath: 'fonts/'
    }
};

const handlebars = {
    test: /\.handlebars$/,
    loader: 'handlebars-loader'
};

module.exports = {
    entry: './src/js/app.js',
    stats: {
        children: true,
        errorDetails: true
    },
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: "js/app.js",
    },
    module: {
        rules: [
            javascriptRules,
            imageRules,
            cssRules,
            fontsRules,
            handlebars
        ]
    },
    plugins: [
        new miniCssExtractPlugin({
            filename: 'css/[name]-styles.css'
        }),
        new copyPlugin({
            patterns: [
                {from: "./src/fonts", to: "fonts"},
            ],
        })
    ],
    optimization: {
        minimize: true,
        minimizer: [
            new CssMinimizerPlugin(),
        ],
    }
};
