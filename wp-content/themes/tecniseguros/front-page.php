<?php
$helper = new Helper();
$image_logo = get_the_post_thumbnail_url($post, 'full', null);
$image_slogan = $helper->getImageMeta($post->ID, 'slogan_image', 'full');
$presentacion = $helper->getLinkPath('presentacion');
?>

<?= get_header() ?>
    <div class="container-fluid screen border">
        <div class="d-flex vh-100 justify-content-center align-items-center flex-column">
            <div class="animate__animated animate__slideInLeft animate__slow">
                <a href="<?= $presentacion ?>">
                    <img class="w-auto" src="<?= $image_logo ?>" alt="">
                </a>
            </div>
            <div class="animate__animated animate__slideInRight animate__slow"
                 style="margin-top: -15px; margin-right: -240px; right: -240px">
                <img class="w-auto mr-4" src="<?= $image_slogan ?>" alt="">
            </div>
        </div>
    </div>
<?= get_footer() ?>