<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <title>
		<?= bloginfo( 'name' ); ?> &raquo; <?= is_front_page() ? bloginfo( 'description' ) : wp_title( '' ); ?>
    </title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="icon" href="<?= get_site_icon_url( 512 ) ?>"/>
    <link rel="icon" href="<?= get_site_icon_url( 192 ) ?>"/>
    <link rel="icon" href="<?= get_site_icon_url( 32 ) ?>"/>

    <link rel="stylesheet"
          href="<?= get_stylesheet_directory_uri() ?>/dist/css/main-styles.css?v=<?= filemtime( get_stylesheet_directory() . '/dist/css/main-styles.css' ) ?>"
          type="text/css"
          media="screen, projection"/>

	<?php wp_head() ?>
</head>
<?php
$style = $class = '';
if ( $args['name'] == 'respaldo-institucional' ) {
	$style = 'min-height: 100vh;';
	$class = 'bg-respaldo-institucional';
}

$lineas_negocio = [
	'seguros-empresariales',
	'seguros-vida-asistencia-medica',
	'seguros-pymes',
    'seguros-individuales',
    'banca-seguros-canales',
    'seguros-credito',
    'fianzas',
];
if ( in_array( $args['name'], $lineas_negocio ) ) {
	$class = 'bg-med-orange';
}

$cx_tecniseguros = [
	'customer-journey',
	'leo-virtual-xperience',
	'doctor-linea',
    'tecniseguros-com-ec',
    '1800-ayuda',
    'tracking-siniestros',
    'charlas-presenciales-online',
    'administracion-contratistas',
    'administracion-riesgos-empresariales',
    'administracion-riesgos-vida-asistencia-medica',
    'certificacion-iso-9001-2015',
];
if ( in_array( $args['name'], $cx_tecniseguros ) ) {
	$class = 'bg-med-red';
}
?>
<body style="<?= $style ?>" class="<?= $class ?>">
