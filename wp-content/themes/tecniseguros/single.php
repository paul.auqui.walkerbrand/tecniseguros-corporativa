<?php
global $post;
get_header( null, [ 'name' => $post->post_name ] );
get_template_part( "templates/single-{$post->post_name}" );
get_footer()
?>