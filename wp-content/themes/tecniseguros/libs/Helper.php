<?php
/**
 * Created by PhpStorm.
 * User: ZBOOK
 * Date: 05/05/2021
 * Time: 11:24
 */

class Helper {
	public function getLinkPath( $path ) {
		return get_page_link( get_page_by_path( $path ) );
	}

	public function getImageAttachement( $post_id, $size ) {
		if ( $img = wp_get_attachment_image_src( $post_id, $size ) ) {
			return $img[0];
		}

		return null;
	}

	public function getImageMeta( $post_id, $key, $size ) {
		$_post = get_post_meta( $post_id, $key, true );

		return $this->getImageAttachement( $_post, $size );
	}

	/**
	 * @param $part
	 * @param null $post_id
	 *
	 * @return array
	 */
	public function getGroupMeta( $part, $post_id = null ) {
		$group = [];
		for ( $i = 1; ; $i ++ ) {
			$node = $this->getPathMeta( "{$part}_{$i}", $post_id );

			if ( ! $node ) {
				break;
			}

			$group[ $i ] = $node;
		}

		return $group;
	}

	public function getPathMeta( $part, $post_id = null ) {
		$info    = [];
		$post_id = $post_id ? $post_id : get_the_ID();

		$group = get_post_meta( $post_id ) ?? [];
		foreach ( $group as $key => $meta ) {
			if ( strpos( $key, $part ) !== false ) {
				$key_ = str_replace( $part . '_', '', $key );
				if ( substr( $key_, 0, 1 ) !== '_' ) {
					$info['ID']    = $post_id;
					$info[ $key_ ] = ( count( $meta ) == 1 ) ? $meta[0] : $meta;

					if ( $key_ == 'imagen' ) {
						$info['image_link'] = $this->getImageMeta( $post_id, $key, 'full' );
					}

					if ( $key_ == 'image' ) {
						$info['img_link'] = $this->getImageMeta( $post_id, $key, 'full' );
					}
				}
			}
		}

		return ( $info ) ? (object) $info : null;
	}

	/**
	 * @param $name
	 * @param null $parent
	 *
	 * @return WP_Post[]
	 */
	public function getPostTypeName( $name, $parent = null ) {
		$_posts = [];
		$args   = [
			'post_type'      => $name,
			'posts_per_page' => - 1,
			'orderby'        => 'menu_order',
			'order'          => 'ASC'
		];

		if ( $parent ) {
			$args['post_parent'] = $parent;
		}

		$key   = 1;
		$posts = new WP_Query( $args );

		if ( $posts->have_posts() ) {
			while ( $posts->have_posts() ) {
				$posts->the_post();
				$_posts[ $key ] = get_post();
				$key ++;
			}
		}

		wp_reset_postdata();

		return $_posts;
	}

	public function getIconsMisionVision( $postMeta ) {
		$ICONS  = [];
		$helper = new Helper();

		if ( $postMeta->icono_1 ) {
			$ICONS[] = [
				'icon'   => $helper->getImageAttachement( $postMeta->icono_1, 'full' ),
				'titulo' => $postMeta->titulo_1,
			];
		}

		if ( $postMeta->icono_2 ) {
			$ICONS[] = [
				'icon'   => $helper->getImageAttachement( $postMeta->icono_2, 'full' ),
				'titulo' => $postMeta->titulo_2,
			];
		}

		if ( $postMeta->icono_3 ) {
			$ICONS[] = [
				'icon'   => $helper->getImageAttachement( $postMeta->icono_3, 'full' ),
				'titulo' => $postMeta->titulo_3,
			];
		}

		if ( $postMeta->icono_4 ) {
			$ICONS[] = [
				'icon'   => $helper->getImageAttachement( $postMeta->icono_4, 'full' ),
				'titulo' => $postMeta->titulo_4,
			];
		}

		if ( $postMeta->icono_5 ) {
			$ICONS[] = [
				'icon'   => $helper->getImageAttachement( $postMeta->icono_5, 'full' ),
				'titulo' => $postMeta->titulo_5,
			];
		}

		return $ICONS;
	}

	public function getStringSinAcentos( $cadena ) {

		//Reemplazamos la A y a
		$cadena = str_replace(
			array( 'Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª' ),
			array( 'A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a' ),
			$cadena
		);

		//Reemplazamos la E y e
		$cadena = str_replace(
			array( 'É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê' ),
			array( 'E', 'E', 'E', 'E', 'e', 'e', 'e', 'e' ),
			$cadena );

		//Reemplazamos la I y i
		$cadena = str_replace(
			array( 'Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î' ),
			array( 'I', 'I', 'I', 'I', 'i', 'i', 'i', 'i' ),
			$cadena );

		//Reemplazamos la O y o
		$cadena = str_replace(
			array( 'Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô' ),
			array( 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o' ),
			$cadena );

		//Reemplazamos la U y u
		$cadena = str_replace(
			array( 'Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û' ),
			array( 'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u' ),
			$cadena );

		//Reemplazamos la N, n, C y c
		$cadena = str_replace(
			array( 'Ñ', 'ñ', 'Ç', 'ç' ),
			array( 'N', 'n', 'C', 'c' ),
			$cadena
		);

		return $cadena;
	}
}