<?php
require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/libs/Helper.php');

$pages = [
    'principal', 'presentacion', 'catalogo',

    'catalogo-estructura-de-servicios',
    'mision-vision-y-adn', 'estructura-de-servicios', 'cobertura-geografica',

    'menu-lineas-de-negocios', 'seguros-empresariales', 'seguros-de-vida-y-asistencia-medica', 'seguros-pymes',
    'seguros-individuales', 'banca-seguros-y-canales', 'seguros-de-credito', 'fianzas',

    'menu-cx-tecniseguros',
    'customer-journey', 'leo-virtual-xperience', 'doctor-en-linea',
    'tecniseguros-web', 'tecnimovil', '1800-ayuda', 'tracking-de-siniestros', 'charlas-presenciales-y-online', 'administracion-de-contratistas',
    'administracion-de-riesgos-empresariales', 'administracion-de-riesgos-vida-y-asistencia-medica', 'certificacion-iso',

    'menu-indices-financieros',
    'indices-financieros', 'nuestros-clientes', 'respaldo-institucional', 'ranking-de-brokers'
];

define('TEMPLATES_TS', $pages);

/**
 * Dump variable.
 */
if (!function_exists('d')) {
    function d()
    {
        call_user_func_array('dump', func_get_args());
    }
}

/**
 * Dump variables and die.
 */
if (!function_exists('dd')) {
    function dd()
    {
        call_user_func_array('dump', func_get_args());
        die();
    }
}

/**
 * Add Image principal en paginas y entradas
 */
function tecnoseguros_setup() {
    add_theme_support( 'post-thumbnails' );
}

add_action( 'after_setup_theme', 'tecnoseguros_setup' );

/**
 * Insertar Javascript js y enviar ruta admin-ajax.php
 */
add_action('wp_enqueue_scripts', 'dcms_insertar_js');
function dcms_insertar_js()
{
    /**
     * 1. Load the datepicker script (pre-registered in WordPress).
     * 2. Script General
     */
    wp_enqueue_script('jquery');
    wp_enqueue_script('dcms_app_theme', get_stylesheet_directory_uri() . '/dist/js/app.js', ['jquery'], filemtime(get_stylesheet_directory() . '/dist/js/app.js'));

	if ( is_singular( 'indice_financiero' ) ) {
		wp_enqueue_script( 'dcms_script_seguro', get_stylesheet_directory_uri() . '/src/js/indice-financiero.js', [ 'jquery' ], '1', true );
	}

	if ( is_page( 'catalogo-cx-tecniseguros' ) ) {
		wp_enqueue_script( 'dcms_script_seguro', get_stylesheet_directory_uri() . '/src/js/cx-tecniseguros.js', [ 'jquery' ], '1', true );
	}
}

/**
 * Registrar menús
 */
function tecni_register_my_menus()
{
    register_nav_menus(
        array(
            'menu-principal' => __('Menú principal'),
            'menu-estructura' => __('Menú estructura de negocios'),
            'menu-lineas' => __('Menú líneas de negocio'),
            'menu-cx' => __('Menú CX Tecniseguros'),
            'menu-indices' => __('Menú índices financieros')
        )
    );
}

add_action('init', 'tecni_register_my_menus');

function add_class_on_li($classes, $item, $args)
{
    if (isset($args->item_class)) {
        $classes[] = $args->item_class;
    }
    return $classes;
}

add_filter('nav_menu_css_class', 'add_class_on_li', 1, 3);