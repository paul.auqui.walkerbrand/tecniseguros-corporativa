<?php
global $post;
//$parentId = $post->post_parent;
//$linkToParent = get_permalink($parentId);
//$category = get_the_category();
$params = array(
  'orderBy' => 't.ID DESC',
  'where' => "category.name = 'Administración de riesgos vida y asistencia médica'"
);

$diapositiva = pods('diapositiva', $params);
?>

<?= get_template_part( 'templates/partial/cx-tecniseguros', null, [
	'diapositiva'  => $diapositiva
] ) ?>