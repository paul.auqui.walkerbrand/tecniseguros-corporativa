<?php
global $post;
$helper   = new Helper();
$catalogo = $helper->getLinkPath( 'catalogo-indices-financieros' );
$items    = $helper->getGroupMeta( 'item', $post->ID );
$image    = $helper->getImageMeta( $post->ID, 'image', 'full' );
?>

<div class="container-fluid screen bg-green-indices-financieros">
    <div class="row">
        <div class="col-12">
            <div class="float-start">
                <img src="<?= $image ?>" alt="Tecniseguros" class="img-fluid p-3">
            </div>
            <div class="">
                <a href="<?= $catalogo ?>"
                   class="d-flex justify-content-center align-items-center text-cyan f-20">
                    <i class="fas fa-chevron-left"></i>
                    <p class="m-0 p-3"><?= __( '&Iacute;NDICES' ) ?> <br> <?= __( 'FINANCIEROS' ) ?></p>
                </a>
            </div>
        </div>
        <div class="col-12 text-center">
            <h1 class="f-88 fw-bold">
                <span
                        class="text-cyan"><? __( '&Iacute;NDICES' ) ?> </span> <?= __( 'FINANCIEROS' ) ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-5 d-flex justify-content-end align-items-center">
            <div class="w-50 text-uppercase h5 menu-indice-financiero">
				<?php
				$img_1     = '';
				foreach ( $items as $key => $item ):
					$name = $helper->getStringSinAcentos( $item->titulo );
					$name  = str_replace( ' ', '-', $name );
					$img_1 = ( $key == 1 ) ? $item->img_link : $img_1;
					?>
                    <div class="py-3 <?= ( $key == 1 ) ? 'active' : '' ?> " data-img="<?= $item->img_link ?>"
                         data-title="<?= $name ?>">
						<?= $item->titulo ?>
                    </div>
				<?php
				endforeach;
				?>
            </div>
        </div>
        <div class="col-7 figure-indice-financiero">
            <div class="img d-flex flex-row">
                <div class="w-75 d-flex align-items-center">
                    <img class="img-fluid" src="<?= $img_1 ?>" alt="Indice Financiero">
                </div>
                <div id="valores" class="text-cyan w-25 d-flex align-items-end justify-content-start my-4 py-5 px-1">
					<?= __( 'Valores expresados en millones de dólares' ) ?>
                </div>
            </div>
        </div>
    </div>
</div>