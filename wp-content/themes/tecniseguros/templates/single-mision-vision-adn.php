<?php
$helper    = new Helper();
$catalogo  = $helper->getLinkPath( 'catalogo-estructura-servicios' );
$servicios = $helper->getGroupMeta( 'seccion', $post->ID );
$logo      = $helper->getImageMeta( $post->ID, 'image', 'full' );
$bg        = get_the_post_thumbnail_url( $post->ID, 'full' );;
?>
<div class="container-fluid screen">
    <div class="row vh-100">
        <div class="col-12">
            <div class="float-start">
                <img src="<?= $logo ?>" alt="Tecniseguros" class="img-fluid p-3">
            </div>
            <div class="">
                <a href="<?= $catalogo ?>"
                   class="d-flex justify-content-center align-items-center text-yellow f-20">
                    <i class="fas fa-chevron-left"></i>
                    <p class="m-0 p-3"><?= __( 'ESTRUCTURA' ) ?><br><?= __( 'DE SERVICIOS' ) ?></p>
                </a>
            </div>
        </div>
        <div class="col-12 text-center">
            <h1 class="f-88 fw-bold">
                <span class="text-yellow"><?= __( 'MISIÓN, VISIÓN Y' ) ?> </span> <?= __( 'ADN' ) ?>
            </h1>
        </div>
        <div class="col-12 text-center bg-mision-vision-adn" style="background-image: url('<?= $bg ?>')">
            <div class="row">
				<?php
				foreach ( $servicios as $servicio ):
					$ICONS = $helper->getIconsMisionVision( $servicio );
					?>
                    <div class="<?= $ICONS ? 'col-12' : 'col-6' ?>">
                        <div class="p-4 w-75 mx-auto">
							<?= $servicio->icono_principal ?>
                            <div class="h2 fw-bold my-3"><?= $servicio->titulo ?></div>
							<?php
							if ( $servicio->descripcion ):
								?>
                                <p class="py-3 h5"><?= $servicio->descripcion ?></p>
							<?php
							endif;
							?>

							<?php
							if ( $ICONS ):
								?>
                                <div class="clearfix"></div>
                                <div id="adn" class=" content-adn col-md-6 h5 text-center offset-3">
                                    <div class="d-flex fs-6 justify-content-between fw-bold">
										<?php
										foreach ( $ICONS as $icon ):
											?>
                                            <div class="">
                                                <img src="<?= $icon['icon'] ?>" alt="<?= $icon['titulo'] ?>"
                                                     class="animate__animated animate__backInLeft animate__delay-1s">
                                                <div class="py-2 title"><?= $icon['titulo'] ?></div>
                                            </div>
										<?php
										endforeach;
										?>
                                    </div>
                                </div>
							<?php
							endif;
							?>
                        </div>
                    </div>
				<?php
				endforeach;
				?>
            </div>
        </div>
    </div>
</div>
<!--</main>-->
<!--</body>-->
<!--<script src="https://code.jquery.com/jquery-3.6.0.min.js"-->
<!--        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>-->

<!--<script type="text/javascript">-->
<!--    // $(document).ready(function () {-->
<!--    //     $(document).on('click', '.btn-mision-vision', function () {-->
<!--    //         var id = $(this).data('content');-->
<!--    //         $('.btn-mision-vision').removeClass('active');-->
<!--    //         $(this).addClass('active');-->
<!--    //-->
<!--    //         $('.content-adn').addClass('d-none');-->
<!--    //         $('#' + id).removeClass('d-none');-->
<!--    //     });-->
<!--    // });-->
<!--</script>-->