<?php
$helper = new Helper();
$catalogo = $helper->getLinkPath('catalogo');
$image_thumnail = get_the_post_thumbnail_url($post->ID, 'full');
$sections = $helper->getGroupMeta('seccion', $post->ID);
?>
<div class="container-fluid screen px-0">
    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            <?php
            foreach ($sections as $key => $section):
                ?>
                <div class="carousel-item <?= ($key == 1) ? 'active' : '' ?> text-center w-100 vh-100 presentacion-slide"
                     style="background-image: url('<?= $section->image_link ?>');">
                    <div class="w-100 vh-100 d-flex flex-column align-content-between justify-content-between">
                        <div class="text-start p-3" style="z-index: 99999;">
                            <a class="text-white" href="<?= $catalogo ?>">
                                <i class="fas fa-home fa-3x "></i>
                            </a>
                        </div>
                        <div class="py-3" style="background-color: <?= $section->color ?>;">
                            <h1 class="text-white fw-bold">
                                <?= $section->texto ?>
                            </h1>
                        </div>
                        <div class="my-3">
                            <img src="<?= $image_thumnail ?>" alt="<?= $section->texto ?>">
                        </div>
                    </div>
                </div>
            <?php
            endforeach;
            ?>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</div>