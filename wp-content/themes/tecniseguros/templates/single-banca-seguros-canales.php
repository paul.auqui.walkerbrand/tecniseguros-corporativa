<?php
global $post;
$parentId = $post->post_parent;
$linkToParent = get_permalink($parentId);
$category = get_the_category();
$params = array(
  'orderBy' => 't.ID DESC',
  'where' => "category.name = 'Banca seguros y canales'"
);

$diapositiva = pods('diapositiva', $params);
?>
<?= get_template_part( 'templates/partial/linea-negocio', null, [
	'diapositiva'  => $diapositiva,
	'title_part_1' => __( 'BANCA SEGUROS Y' ),
	'title_part_2' => __( 'CANALES' ),
] ) ?>
