<?php
global $post;
$helper   = new Helper();
$catalogo = $helper->getLinkPath( 'catalogo-indices-financieros' );
$image    = $helper->getImageMeta( $post->ID, 'image', 'full' );
$text     = get_post_meta( $post->ID, 'poliza_responsabilidad_civil', true );
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="float-start">
                <img src="<?= $image ?>" alt="Tecniseguros" class="img-fluid p-3">
            </div>
            <div class="">
                <a href="<?= $catalogo ?>"
                   class="d-flex justify-content-center align-items-center text-cyan f-20">
                    <i class="fas fa-chevron-left"></i>
                    <p class="m-0 p-3"><?= __( 'ÍNDICES' ) ?> <br> <?= __( 'FINANCIEROS' ) ?></p>
                </a>
            </div>
        </div>
        <div class="col-12 text-center">
            <h1 class="f-88 fw-bold text-white">
                <span class="text-cyan"><?= __( 'RESPALDO' ) ?> </span> <?= __( 'INSTITUCIONAL' ) ?>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4">
			<?= $post->post_content ?>

            <div class="text-white p-2 bg-cyan w-75 h6 mb-0 border-tab-title">
                <strong><?= __( 'PÓLIZA DE RESPONSABILIDAD CIVIL' ) ?></strong>
            </div>
            <div class="text-cyan border-cyan p-3 border-tab h6">
				<?= $text ?>
            </div>
        </div>
    </div>
</div>
<indice-script></indice-script>