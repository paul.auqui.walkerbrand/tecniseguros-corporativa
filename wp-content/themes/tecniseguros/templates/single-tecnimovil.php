<?php
global $post;
$helper = new Helper();
$back   = $helper->getLinkPath( 'catalogo-cx-tecniseguros' );

$params        = array(
	'orderBy' => 't.ID DESC',
);
$tecnimovil    = pods( 'tecnimovil', $params );
$tecnimovilder = pods( 'tecnimovil', $params );
?>

<div class="container-fluid  screen">
    <div class="row bg-red pb-2">
        <div class="col-12">
            <div class="float-start">
                <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo_tecni_blanco.png" alt=""
                     class="img-fluid p-3">
            </div>
            <div class="">
                <a href="<?= $back ?>" class="d-flex justify-content-center align-items-center text-white f-20">
                    <i class="fas fa-chevron-left"></i>
                    <p class="m-0 p-3"><?= __( 'CX' ) ?><br><?= __( 'TECNISEGUROS' ) ?></p>
                </a>
            </div>
        </div>
        <div class="col-12 text-center">
            <h1 class="f-title fw-bold animate__animated animate__fadeInDownBig"><span
                        class="text-white"><?= __( 'TECNIMÓVIL' ) ?></span></h1>
        </div>
    </div>

    <!-- Acordión -->
    <div class="row py-4 px-5 text-black accordion accordion-flush bg-tecnimovil h-100" id="tecnimovil">
        <div class="col-6">
			<?php
			if ( 0 < $tecnimovil->total() ) :
				while ( $tecnimovil->fetch() ) :
					if ( $tecnimovil->field( 'lado' ) == 'Izquierda' ) :
						?>
                        <div class="row m-2 animate__animated animate__zoomInDown animate__animated animate__zoomInDown">
                            <div class="col-1 rounded-start accordion-item bg-acordion text-white f-40 d-flex justify-content-center align-items-center collapsed"
                                 data-bs-toggle="collapse"
                                 data-bs-target="#<?php echo $tecnimovil->display( 'name' ) ?>" aria-expanded="false">
                                <i class="far fa-check-circle"></i>
                            </div>
                            <div class="col-11 py-2 rounded-end accordion-item animate__animated animate__shakeY">
                                <button class="accordion-button collapsed text-black f-26 fw-bold" type="button"
                                        data-bs-toggle="collapse"
                                        data-bs-target="#<?php echo $tecnimovil->display( 'slug' ) ?>"
                                        aria-expanded="false">
									<?php echo $tecnimovil->display( 'title' ) ?>
                                </button>
                                <div id="<?php echo $tecnimovil->display( 'slug' ) ?>"
                                     class="accordion-collapse collapse animate__animated animate__pulse"
                                     data-bs-parent="#tecnimovil">
                                    <div class="accordion-body f-18">
										<?php echo $tecnimovil->display( 'content' ) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
					<?php
					endif;
				endwhile;
			endif;
			?>
        </div>
        <div class="col-6 accordion">
			<?php
			if ( 0 < $tecnimovilder->total() ) :
				while ( $tecnimovilder->fetch() ) :
					if ( $tecnimovilder->field( 'lado' ) == 'Derecha' ) :
						?>
                        <div class="row m-2 animate__animated animate__zoomInDown animate__animated animate__zoomInDown">
                            <div class="col-1 rounded-start accordion-item bg-acordion text-white f-40 d-flex justify-content-center align-items-center collapsed"
                                 data-bs-toggle="collapse"
                                 data-bs-target="#<?php echo $tecnimovilder->display( 'name' ) ?>"
                                 aria-expanded="false">
                                <i class="far fa-check-circle"></i>
                            </div>
                            <div class="col-11 py-2 rounded-end accordion-item animate__animated animate__shakeY">
                                <button class="accordion-button collapsed text-black f-26 fw-bold" type="button"
                                        data-bs-toggle="collapse"
                                        data-bs-target="#<?php echo $tecnimovilder->display( 'slug' ) ?>"
                                        aria-expanded="false">
									<?php echo $tecnimovilder->display( 'title' ) ?>
                                </button>
                                <div id="<?php echo $tecnimovilder->display( 'slug' ) ?>"
                                     class="accordion-collapse collapse animate__animated animate__pulse"
                                     data-bs-parent="#tecnimovil">
                                    <div class="accordion-body f-18">
										<?php echo $tecnimovilder->display( 'content' ) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
					<?php
					endif;
				endwhile;
			endif;
			?>
        </div>
    </div>
</div>