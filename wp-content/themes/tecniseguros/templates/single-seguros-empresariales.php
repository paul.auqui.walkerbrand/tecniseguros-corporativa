<?php
global $post;
$params   = array(
	'orderBy' => 't.ID DESC',
	'where'   => "category.name = 'Seguros empresariales'"
);

$diapositiva = pods( 'diapositiva', $params );
?>

<?= get_template_part( 'templates/partial/linea-negocio', null, [
	'diapositiva'  => $diapositiva,
	'title_part_1' => __( 'SEGUROS' ),
	'title_part_2' => __( 'EMPRESARIALES' ),
] ) ?>
