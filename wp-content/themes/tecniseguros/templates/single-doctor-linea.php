<?php
global $post;
$params = array(
  'orderBy' => 't.ID DESC',
  'where' => "category.name = 'Doctor en línea'"
);

$diapositiva = pods('diapositiva', $params);
?>

<?= get_template_part( 'templates/partial/cx-tecniseguros', null, [
	'diapositiva'  => $diapositiva
] ) ?>
