<?php
global $post;
$helper = new Helper();
$back   = $helper->getLinkPath( 'catalogo-cx-tecniseguros' );
$params = array(
	'orderBy' => 't.ID DESC',
	'where'   => "category.name = 'Customer journey'"
);

$diapositiva = pods( 'diapositiva', $params );
?>

<?= get_template_part( 'templates/partial/cx-tecniseguros', null, [
	'diapositiva'  => $diapositiva
] ) ?>
