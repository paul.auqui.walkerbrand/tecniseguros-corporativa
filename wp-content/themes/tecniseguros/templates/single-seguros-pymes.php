<?php
global $post;
//$parentId = $post->post_parent;
//$linkToParent = get_permalink($parentId);
//$category = get_the_category();
$params = array(
  'orderBy' => 't.ID DESC',
  'where' => "category.name = 'Seguros pymes'"
);

$diapositiva = pods('diapositiva', $params);
?>

<?= get_template_part( 'templates/partial/linea-negocio', null, [
	'diapositiva'  => $diapositiva,
	'title_part_1' => __( 'SEGUROS' ),
	'title_part_2' => __( 'PYMES' ),
] ) ?>
