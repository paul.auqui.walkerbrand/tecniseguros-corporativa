<?php
$helper        = new Helper();
$catalogo      = $helper->getLinkPath( 'catalogo' );
$lineasNegocio = $helper->getPostTypeName( 'linea_negocio' );
?>
<section class="container-fluid">
    <div class="row">
        <div class=" col-md-3  vh-100 bg-lineas p-3 d-flex flex-column justify-content-between ">
            <div class="">
                <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo.png" alt="LOGO"/>
            </div>

            <div class="px-5 animate__animated animate__backInLeft">
                <a href="<?= $catalogo ?>" class="text-white">
                    <i class="fas fa-chevron-left"></i>
                    <h1 class="fw-bold">LÍNEAS</h1>
                    <h2 class="fw-bold">DE NEGOCIO</h2>
                </a>
            </div>

            <div class="">&nbsp;</div>
        </div>
        <div class="col-md-9 bg-menu-lineas">
            <div class="row">
                <div class="col-md-6 vh-100 p-5">
                    <div class="menu-lineas d-flex justify-content-evenly flex-column ">
						<?php
						foreach ( $lineasNegocio as $lineanegocio ):
							$link = get_post_permalink( $lineanegocio->ID );
							?>
                            <div class="animate__animated animate__jackInTheBox">
                                <a href="<?= $link ?>" data-content="<?= $lineanegocio->post_name ?>">
									<?= $lineanegocio->post_title ?>
                                </a>
                            </div>
						<?php
						endforeach;
						?>
                    </div>
                </div>
                <div class=" col-md-6 vh-100 d-flex flex-column justify-content-center align-items-end">
                    <div class="lineas-figure d-flex justify-content-end">
                        <div class=" bg-lineas-color d-flex justify-content-center align-items-center">
							<?php
							foreach ( $lineasNegocio as $lineanegocio ):
								?>
                                <p class="f-22 m-3 d-none content-lineas-negocio" id="<?= $lineanegocio->post_name ?>">
									<?= $lineanegocio->post_content ?>
                                </p>
							<?php
							endforeach;
							?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".menu-lineas a")
            .mouseenter(function () {
                var content = $(this).data("content");
                $(".content-lineas-negocio").addClass("d-none");
                $("#" + content).removeClass("d-none");
            })
            .mouseleave(function () {
                //$('.content-lineas-negocio').addClass('d-none');
            });
    });
</script>