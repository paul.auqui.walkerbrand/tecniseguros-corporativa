<?php
global $post;
$helper         = new Helper();
$catalogo       = $helper->getLinkPath( 'catalogo' );
$cxTecniseguros = $helper->getPostTypeName( 'cx_tecniseguro' );
?>

<section class="container-fluid">
    <div class="row">
        <div class="col-md-3 vh-100 bg-cx p-3 d-flex flex-column justify-content-between">
            <div class="">
                <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo.png" alt="LOGO">
            </div>
            <div class="px-5 animate__animated  animate__backInLeft">
                <a href="<?= $catalogo ?>" class="text-white">
                    <i class="fas fa-chevron-left"></i>
                    <h1 class="fw-bold"><?= __( 'CX' ) ?></h1>
                    <h2 class="fw-bold"><?= __( 'TECNISEGUROS' ) ?></h2>
                </a>
            </div>
            <div class="">&nbsp;</div>
        </div>
        <div class="col-md-9 bg-menu-cx">
            <div class="row">
                <div class="col-md-6 vh-100 p-5">
                    <div class="menu-cx d-flex justify-content-between flex-column ">
						<?php
						foreach ( $cxTecniseguros as $cxTecniseguro ):
							$sub_titulo = get_post_meta( $cxTecniseguro->ID, 'sub_titulo', true );
							$link = get_post_permalink( $cxTecniseguro->ID );
							$img = get_the_post_thumbnail_url( $cxTecniseguro->ID );
							?>
                            <div class="animate__animated animate__jackInTheBox">
                                <a href="<?= $link ?>" data-logo="<?= $img ?>">
                                    <div><?= $cxTecniseguro->post_title ?></div>
                                    <div class="normal"><?= $sub_titulo ?></div>
                                </a>
                            </div>
						<?php
						endforeach;
						?>
                    </div>
                </div>
                <div class="col-md-6 vh-100 d-flex flex-column justify-content-center align-items-end">
                    <div class="cx-figure d-flex justify-content-end">
                        <div class="cx-figure-img bg-cx-color d-flex justify-content-center align-items-center">
                            <!--<img src="" class="logo d-none" alt="LOGO">-->
                            <img src="" class="logo w-auto d-none" alt="LOGO">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<cx-script></cx-script>