<?php
global $post;
$helper = new Helper();
$back   = $helper->getLinkPath( 'catalogo-cx-tecniseguros' );
?>

<div class="container-fluid screen position-relative m-0 p-0">
    <div class="position-absolute w-100" style="z-index: 10;">
        <div class="float-start">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo_tecni_blanco.png" alt=""
                 class="img-fluid p-3">
        </div>
        <div class="text-center w-100">
            <a href="<?= $back ?>" class="d-flex justify-content-center align-items-center text-white f-20">
                <i class="fas fa-chevron-left"></i>
                <p class="m-0 p-3"><?= __( 'CX' ) ?><br><?= __( 'TECNISEGUROS' ) ?></p>
            </a>
            <h1 class="f-88 fw-bold animate__animated animate__fadeInDownBig">
                <span class="text-white"><?= __( '1800 TU AYUDA' ) ?></span>
            </h1>
        </div>
    </div>

    <div class="position-absolute bg-red border w-100 vh-100 top-0" style="z-index: 1;">
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active bg-1800-slide w-100 vh-100"
                     style="background-image: url(<?= get_stylesheet_directory_uri() ?>/dist/static/1800-ayuda-1-new.jpg)">
                </div>
                <div class="carousel-item bg-1800-slide w-100 vh-100"
                     style="background-image: url(<?= get_stylesheet_directory_uri() ?>/dist/static/1800-ayuda-2-new.png)">
                </div>
                <div class="carousel-item bg-1800-slide w-100 vh-100"
                     style="background-image: url(<?= get_stylesheet_directory_uri() ?>/dist/static/1800-ayuda-3-new.jpg)">
                </div>
                <div class="carousel-item bg-1800-slide w-100 vh-100"
                     style="background-image: url(<?= get_stylesheet_directory_uri() ?>/dist/static/1800-ayuda-4-new.jpg)">
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
                    data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
                    data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
</div>