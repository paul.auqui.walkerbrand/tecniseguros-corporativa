<?php
global $post;
$helper       = new Helper();
$catalogo     = $helper->getLinkPath( 'catalogo-estructura-servicios' );
$logo         = $helper->getImageMeta( $post->ID, 'image', 'full' );
$img_servicio = $helper->getImageMeta( $post->ID, 'servicio', 'full' );
$img_empresa  = $helper->getImageMeta( $post->ID, 'empresa', 'full' );
$img_persona  = $helper->getImageMeta( $post->ID, 'personas', 'full' );
?>

<div class="container-fluid screen">
    <div class="row">
        <div class="col-12">
            <div class="float-start">
                <img src="<?= $logo ?>" alt="Tecniseguros" class="img-fluid p-3">
            </div>
            <div class="">
                <a href="<?= $catalogo ?>" class="d-flex justify-content-center align-items-center text-yellow f-20">
                    <i class="fas fa-chevron-left"></i>
                    <p class="m-0 p-3"><?= __( 'ESTRUCTURA' ) ?> <br> <?= __( 'DE SERVICIOS' ) ?></p>

                </a>
            </div>
        </div>
    </div>
    <div class="row my-5">
        <div class="col p-5 d-flex justify-content-center align-items-center">
            <div class="text-start animate__animated animate__backInLeft">
                <h1 class="f-88 fw-bold">
                    <span class="text-yellow"><?= __( 'ESTRUCTURA' ) ?> </span> <?= __( 'DE SERVICIOS' ) ?>
                </h1>
            </div>
        </div>
        <div class="col p-5 text-center position-relative animate__animated animate__fadeInRight">
            <div class="content-est-vision d-flex flex-column justify-content-center"
                 style="background-image: url('<?= $img_servicio ?>')">
                <div class="d-flex justify-content-between fs-5">
                    <a class="link py-3 fw-bold"
                       data-img="<?= $img_empresa ?>"
                       data-text="empresas">
                        Empresas
                    </a>
                    <a class="link py-3 fw-bold"
                       data-img="<?= $img_persona ?>"
                       data-text="personas" style="margin-right: -20px;">
                        Personas
                    </a>
                </div>
                <div class="text-est-vision">
                    <img class="empresas d-none"
                         src="<?= get_stylesheet_directory_uri() ?>/dist/static/text-empresas.png" alt="Empresa">
                    <img class="personas d-none"
                         src="<?= get_stylesheet_directory_uri() ?>/dist/static/text-personas.png"
                         style="margin-left: -62px;" alt="">
                </div>
            </div>

        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '.content-est-vision .link', function () {
            var url = $(this).data('img');
            var text = $(this).data('text');
            $('.content-est-vision').css('background-image', 'url(' + url + ')');
            $(this).addClass('active');

            $('.text-est-vision img').addClass('d-none');
            $('.text-est-vision .' + text).removeClass('d-none');
            console.log(url, text);
        });
    });
</script>