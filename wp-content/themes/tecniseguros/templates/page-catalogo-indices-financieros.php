<?php
global $post;
$helper   = new Helper();
$catalogo = $helper->getLinkPath( 'catalogo' );
$indices  = $helper->getPostTypeName( 'indice_financiero' );
?>

<section class="container-fluid">
    <div class="row">
        <div class="col-md-3 vh-100 bg-indices p-3 d-flex flex-column justify-content-between">
            <div class="">
                <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo.png" alt="LOGO">
            </div>
            <div class="px-5 animate__animated  animate__backInLeft">
                <a href="<?= $catalogo ?>" class="text-white">
                    <i class="fas fa-chevron-left"></i>
                    <h1 class="fw-bold"><?= __( '&Iacute;NDICES' ) ?></h1>
                    <h2 class="fw-bold"><?= __( 'FINANCIEROS' ) ?></h2>
                </a>
            </div>
            <div class="">&nbsp;</div>
        </div>
        <div class="col-md-9 vh-100 bg-indices-financiero">
            <div class="container-fluid ">
                <div class="row screen">
					<?php
					foreach ( $indices as $key => $indice ):
						$name = str_replace( ' ', '-', trim( $indice->post_name ) );
						$img_menu = get_the_post_thumbnail_url( $indice->ID, 'full' );
						$img_menu_over = $helper->getImageMeta( $indice->ID, 'menu_over', 'full' );
						$link = get_the_permalink( $indice );
						$position = '';
						switch ( $key ) {
							case 1:
								$position = 'justify-content-end align-items-end';
								break;
							case 2:
								$position = 'justify-content-end align-items-start';
								break;
							case 3:
								$position = 'justify-content-start align-items-end';
								break;
							case 4:
								$position = 'justify-content-start align-items-start';
								break;
						}
						?>
                        <div class="col-6 d-flex <?= $position ?> flex-column">
                            <div class="m-4 menu-indice animate__animated animate__rotateInDownLeft">
                                <a href="<?= $link ?>">
                                    <style scope>
                                        .<?=$name?>:hover {
                                            background-image: url("<?=$img_menu_over?>") !important;
                                        }
                                    </style>
                                    <div class="border <?= $name ?> figure w-100"
                                         style="background-image: url('<?= $img_menu ?>')">

                                    </div>
                                    <div class="text-center h6 p-2 text-uppercase">
										<?= $indice->post_title ?>
                                    </div>
                                </a>
                            </div>
                        </div>
					<?php
					endforeach;
					?>
                </div>
            </div>
        </div>
    </div>
</section>