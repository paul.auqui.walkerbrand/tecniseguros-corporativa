<?php
$helper = new Helper();
$menus = $helper->getGroupMeta('menu', $post->ID);
?>
<div class="container-fluid ">
    <div class="row vh-100">
        <?php
        foreach ($menus as $key => $menu):
            $page_link = get_the_permalink($menu->link);
            ?>
            <style scope>
                #catalogo-<?= $key ?>:hover {
                    background: url("<?= $menu->image_link ?>"), <?= $menu->color_over ?> !important;
                    background-size: cover !important;
                }
            </style>
            <div id="catalogo-<?= $key ?>" class="col-6 p-0 border-3 border-gray catalogo"
                 style="background: linear-gradient(0deg, rgba(251, 251, 251, 0.85), rgba(251, 251, 251, 0.85)), url('<?= $menu->image_link ?>')">
                <a href="<?= $page_link ?>"
                   class="h-100 d-flex align-items-center justify-content-center position-relative">
                    <div class="text-center animate__animated  animate__bounceInDown">
                        <h2 class="f-56 fw-bold"><?= $menu->titulo ?></h2>
                        <p class="f-40 fw-bold"><?= $menu->subtitulo ?></p>
                    </div>
                </a>
            </div>
        <?php
        endforeach;
        ?>
    </div>
</div>