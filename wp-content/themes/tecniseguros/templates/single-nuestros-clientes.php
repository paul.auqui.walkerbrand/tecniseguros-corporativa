<?php
global $post;
$helper   = new Helper();
$catalogo = $helper->getLinkPath( 'catalogo-indices-financieros' );
$clientes = $helper->getGroupMeta( 'cliente', $post->ID );
$image    = $helper->getImageMeta( $post->ID, 'image', 'full' );
?>

<div class="container-fluid screen bg-green-indices-financieros">
    <div class="row">
        <div class="col-12">
            <div class="float-start">
                <img src="<?= $image ?>" alt="Tecniseguros" class="img-fluid p-3">
            </div>
            <div class="">
                <a href="<?= $catalogo ?>"
                   class="d-flex justify-content-center align-items-center text-cyan f-20">
                    <i class="fas fa-chevron-left"></i>
                    <p class="m-0 p-3">
						<?= __( 'ÍNDICES' ) ?> <br> <?= __( 'FINANCIEROS' ) ?>
                    </p>
                </a>
            </div>
        </div>
        <div class="col-12 text-center">
            <h1 class="f-88 fw-bold">
                <span class="text-cyan"><?= __( 'NUESTROS' ) ?> </span> <?= __( 'CLIENTES' ) ?>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div id="carouselExampleCaptions" class="carousel slide " data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"
                            aria-current="true" aria-label="Slide 1"></button>

                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                            aria-label="Slide 2"></button>
                </div>
                <div class="carousel-inner  ">
					<?php
					foreach ( $clientes as $key => $cliente ):
						$imagen = $helper->getImageAttachement( $cliente->{'cliente_' . $key}, 'full' );
						?>
                        <div class="carousel-item <?= ( $key == 1 ) ? 'active' : '' ?> text-center my-5">
                            <img src="<?= $imagen ?>" class="pr-0 mx-auto" alt="Nuestros clientes">
                        </div>
					<?php
					endforeach;
					?>
                </div>

                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                        data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                        data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    </div>
</div>

<indice-script></indice-script>