<?php
global $post;
$helper = new Helper();
$back   = $helper->getLinkPath( 'catalogo-cx-tecniseguros' );
?>

<div class="container-fluid screen">
    <div class="row">
        <div class="col-12">
            <div class="float-start">
                <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo_tecni_rojo.png" alt=""
                     class="img-fluid p-3">
            </div>
            <div class="">
                <a href="<?= $back ?>"
                   class="d-flex justify-content-center align-items-center text-red f-20">
                    <i class="fas fa-chevron-left"></i>
                    <p class="m-0 p-3">
                        <?= __( 'CX' ) ?><br><?= __( 'TECNISEGUROS' ) ?>
                    </p>
                </a>
            </div>
        </div>
        <div class="col-12 text-center">
            <h1 class="f-title fw-bold">
                <span class="text-red"><?= __( 'TRACKING DE' ) ?></span> <?= __( 'SINIESTROS' ) ?>
            </h1>
        </div>
    </div>
    <!-- Contenido -->
    <div class="row p-5">
        <div class="col-3 d-flex justify-content-center align-items-center animate__animated animate__rubberBand">
            <p class="f-24 text-center">Conoce el estado de tu siniestro o <br> reembolso en tiempo real.</p>
        </div>
        <div class="col-3 d-flex justify-content-center align-items-center animate__animated animate__rubberBand">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/tracking-siniestros-1.png" class="img-fluid"
                 alt="">
        </div>
        <div class="col-3 d-flex justify-content-center align-items-center animate__animated animate__rubberBand">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/tracking-siniestros-2.png" class="img-fluid"
                 alt="">
        </div>
        <div class="col-3 d-flex justify-content-center align-items-center animate__animated animate__rubberBand">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/tracking-siniestros-3.png" class="img-fluid"
                 alt="">
        </div>
    </div>
</div>