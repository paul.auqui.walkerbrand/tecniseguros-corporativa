<?php
global $post;
$helper = new Helper();
//$parentId     = $post->post_parent;
//$linkToParent = get_permalink( $parentId );
$back    = $helper->getLinkPath( 'catalogo-cx-tecniseguros' );
$imagen  = $helper->getImageMeta( $post->ID, 'imagen', 'full' );
$imagen2 = $helper->getImageMeta( $post->ID, 'imagen_2', 'full' );
?>
<body class="bg-med-red">
<main>
    <div class="container-fluid screen">
        <div class="row">
            <div class="col-12">
                <div class="float-start">
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo_tecni_rojo.png" alt=""
                         class="img-fluid p-3">
                </div>
                <div class="">
                    <a href="<?= $back ?>"
                       class="d-flex justify-content-center align-items-center text-red f-20">
                        <i class="fas fa-chevron-left"></i>
                        <p class="m-0 p-3">CX <br> TECNISEGUROS</p>
                    </a>
                </div>
            </div>
            <div class="col-12 text-center">
                <h1 class="f-title fw-bold"><span class="text-red">CERTIFICACIÓN ISO </span> 9001:2015</h1>
            </div>
        </div>
        <!-- Contenido -->
        <div class="row text-center p-4">
            <div class="col-6">
                <img src="<?= $imagen ?>" class="border img-fluid" alt="Certificación ISO 9001:2015">
            </div>
            <div class="col-6">
                <img src="<?= $imagen2 ?>" class="border img-fluid"
                     alt="Certificación ISO 9001:2015">
            </div>
        </div>
    </div>
</main>
</body>