<?php
$helper = new Helper();
$catalogo = $helper->getLinkPath('catalogo');
$servicios = $helper->getPostTypeName('estructura-servicio');
?>

<section class="container-fluid">
    <div class="row">
        <div class="col-md-3 vh-100 bg-estructura p-3 d-flex flex-column justify-content-between">
            <div class="">
                <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo.png" alt="LOGO">
            </div>

            <div class="px-5 animate__animated  animate__backInLeft">
                <a href="<?= $catalogo ?>" class="text-white">
                    <i class="fas fa-chevron-left"></i>
                    <h1 class="fw-bold"><?= __('ESTRUCTURA') ?></h1>
                    <h3><?= __('DE SERVICIOS') ?></h3>
                </a>
            </div>

            <div class="">&nbsp;</div>
        </div>
        <div class="col-md-9 vh-100 bg-estructura-servicios">
            <div class="container-fluid">
                <div class="row screen">
                    <?php
                    foreach ($servicios as $key => $servicio):
                        $link = get_the_permalink($servicio->ID);
                        $icono = get_post_meta($servicio->ID, 'icono', true);
                        $video = get_post_meta($servicio->ID, 'video', true);

                        switch ($key) {
                            case 1:
                                $position = 'justify-content-end align-items-end';
                                break;
                            case 2:
                                $position = 'justify-content-start align-items-end';
                                break;
                            case 3:
                                $position = 'justify-content-end align-items-start';
                                break;
                            case 4:
                                $position = 'justify-content-start align-items-start';
                                break;
                            default:
                                $position = 'justify-content-end align-items-end';
                        }
                        ?>
                        <div class="col-md-6 d-flex <?= $position ?> flex-row">
                            <div class="menu-estructura">
                                <a href="<?= $video ? $video : $link ?>" target="<?= $video ? '_blank' : '_self' ?>">
                                    <div class="figure w-100 d-flex justify-content-center align-items-center flex-row">
                                        <?= $icono ?>
                                    </div>
                                    <div class="text-center h6 px-2 py-4">
                                        <?= $servicio->post_title ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php
                    endforeach;
                    ?>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>