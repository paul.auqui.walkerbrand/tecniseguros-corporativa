<?php
global $post;
$helper   = new Helper();
$catalogo = $helper->getLinkPath( 'catalogo-lineas-negocios' );
$diapositiva  = isset( $args['diapositiva'] ) ? $args['diapositiva'] : [];
$title_part_1 = isset( $args['title_part_1'] ) ? $args['title_part_1'] : null;
$title_part_2 = isset( $args['title_part_2'] ) ? $args['title_part_2'] : null;
?>
<!--<div class="bg-med-orange">-->
<div class="container-fluid screen">
    <div class="row h-auto">
        <div class="col-12">
            <div class="float-start">
                <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo_tecni_orange.png" alt=""
                     class="img-fluid p-3">
            </div>
            <div class="f-18">
                <a href="<?= $catalogo ?>"
                   class="d-flex justify-content-center align-items-center text-orange f-20">
                    <i class="fas fa-chevron-left"></i>
                    <p class="m-0 p-3"><?= __( 'LÍNEAS DE' ) ?> <br> <?= __( 'TECNISEGUROS' ) ?></p>
                </a>
            </div>
        </div>
    </div>
    <div class="row h-auto">
        <div class="col-12 text-center ">
            <h1 class="f-title fw-bold animate__animated animate__lightSpeedInRight">
                <span class="text-orange"><?= $title_part_1 ?></span> <?= $title_part_2 ?>
            </h1>
        </div>
    </div>
    <!-- Contenido -->
    <div class="row h-90">
        <div class="col-12 carousel slide" id="slider" data-bs-ride="carousel">
            <!-- Slider -->
            <div class="carousel-inner">
				<?php
				$c     = 0;
				$class = '';

				if ( 0 < $diapositiva->total() ) :
					while ( $diapositiva->fetch() ) :
						$c ++;
						$class = ( $c == 1 ) ? 'active' : '';

						?>
                        <div class="carousel-item <?php echo $class; ?>">
                            <img src="<?php echo $diapositiva->display( 'imagen' ) ?>"
                                 class="img-fluid w-100 height-banner m-0 text-end" alt="">
                            <div class="row">
                                <div class="col-8 p-5">
                                    <h3 class="text-orange text-center f-26 fw-bold"><?php echo $diapositiva->display( 'titulo_contenido' ) ?></h3>
                                    <p class="f-20 fw-bold py-3"><?php echo $diapositiva->display( 'subtitulo' ) ?></p>
									<?php echo $diapositiva->display( 'contenido_blanco' ) ?>
                                </div>
                                <div class="col-4 p-5 text-white">
									<?php echo $diapositiva->display( 'contenido_color' ) ?>
                                </div>
                            </div>
                        </div>
					<?php endwhile;
				endif; ?>
            </div>

            <!-- Controles circulares -->
            <div class="carousel-indicators">
				<?php
				$c      = 0;
				$class  = '';
				$bullet = $diapositiva->total();

				if ( $bullet > 1 ) {
					for ( $i = 0; $i < $bullet; $i ++ ) {
						$class = ( $c == 0 ) ? 'active' : '';
						?>
                        <button type="button" data-bs-target="#slider" data-bs-slide-to="<?php echo $c ?>"
                                class="<?php echo $class; ?>" aria-label="Slide <?php echo $c ?>"></button>
						<?php
						$c ++;
					}
				}
				?>
            </div>
        </div>
    </div>
</div>
<!--</div>-->