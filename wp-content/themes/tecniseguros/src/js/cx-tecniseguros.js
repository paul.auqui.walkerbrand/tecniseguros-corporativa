jQuery(document).ready(function ($) {
    $(".menu-cx a").mouseenter(function () {
        var logo = $(this).data('logo');
        var image = $(this).data('image');

        if (image) {
            $('.cx-figure-img')
                .css('background-image', 'url(' + image + ')');
        }

        if (logo) {
            $('.cx-figure-img .logo')
                .removeClass('d-none')
                .attr('src', logo)
                .addClass('animate__animated')
                .addClass('animate__fadeIn');
        }

        $(this).addClass('active');
    }).mouseleave(function () {
        $('.cx-figure-img')
            .attr('style', null);

        $('.cx-figure-img .logo')
            .addClass('d-none')
            .attr('src', '')
            .removeClass('animate__animated')
            .removeClass('animate__fadeIn');

        $(this).removeClass('active');
    });
});
