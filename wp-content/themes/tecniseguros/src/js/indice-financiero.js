jQuery(document).ready(function ($) {
    $(document).on('click', ".menu-indice-financiero div", function () {
        var image = $(this).data('img');
        var title = $(this).data('title');

        if (image) {
            $('.figure-indice-financiero img').attr('src', image);
        }

        $('#valores').removeClass('d-none');
        $('.menu-indice-financiero div').removeClass('active');
        $(this).addClass('active');

        console.log(title);
        if (title == 'INDICE-DE-LIQUIDEZ') {
            $('#valores').addClass('d-none');
        }
    });
});
