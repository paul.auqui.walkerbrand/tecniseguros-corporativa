<?php
/**
 * Template Name: Search Page
 */
get_header();

$helper = new helper();
global $wp_query;
?>
    <div class="container my-5">
        <div class="py-5">
            <h4 class="fw-bold mt-4">Resultados de la busqueda</h4>

			<?php
			if ( $post_search = $helper->getPostsSearchType() ) {
				foreach ( $post_search as $_post_type => $_post ) {
					$parent = $_post['parent'];
					$__link = $parent['link'];
					$__post = $parent['post'];
					?>
                    <div class="border-search border-radius-8 p-3 my-3">
                        <div class="my-3">
                            <a href="<?= $__link ?>" class="nav-link d-block py-0">
								<?= mb_strtoupper( $__post->post_type ) ?>
                            </a>
                            <label for="" class="text-muted nav-link py-0">
								<?= mb_strtoupper( $__post->post_title ) ?>
                            </label>
                        </div>

                        <div class="mx-4 pl-5">
							<?php
							foreach ( $_post['search'] as $search ) {
								$__link = $search['link'];
								$__post = $search['post'];
								?>
                                <div class="my-2 small">
                                    <a href="<?= $__link ?>" class="nav-link d-block py-0">
										<?= mb_strtoupper( $__post->post_type ) ?>
                                    </a>
                                    <label for="" class="text-muted nav-link py-0">
										<?= mb_strtoupper( $__post->post_title ) ?>
                                    </label>
									<?php //d( $parent ) ?>
                                </div>
								<?php
							}
							?>
                        </div>

                    </div>
					<?php
				}
			}
			?>
        </div>
    </div>
<?php
get_footer();